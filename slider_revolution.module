<?php
/**
 * @file
 * Provide a Slider Revolution fieldable panels pane.
 */

/**
 * Implements hook_library().
 */
function slider_revolution_libraries_info() {
  $libraries['slider_revolution'] = array(
    'name' => 'Slider revolution',
    'vendor url' => 'http://www.themepunch.com/codecanyon/revolution_wp/',
    'download url' => 'http://codecanyon.net/item/slider-revolution-responsive-jquery-plugin/2580848',
    'path' => 'source/rs-plugin',
    'version arguments' => array(
      'file' => 'source/rs-plugin/js/jquery.themepunch.revolution.min.js',
      'pattern' => '@version:\s+([0-9a-zA-Z\.-]+)@',
      'lines' => 7,
      'cols' => 100,
    ),
    'files' => array(
      'js' => array(
        'js/jquery.themepunch.plugins.min.js',
        'js/jquery.themepunch.revolution.min.js',
      ),
      'css' => array(
        'css/settings.css',
      ),
    ),
    'variants' => array(
      'no-css' => array(
        'files' => array(
          'js' => array(
            'js/jquery.themepunch.plugins.min.js',
            'js/jquery.themepunch.revolution.min.js',
          ),
        ),
        'variant arguments' => array(
          'variant' => 'no-css',
        ),
      ),
      'source' => array(
        'files' => array(
          'js' => array(
            'js/jquery.themepunch.plugins.min.js',
            'js/jquery.themepunch.revolution.js',
          ),
          'css' => array(
            'css/settings.css',
          ),
        ),
        'variant arguments' => array(
          'variant' => 'source',
        ),
      ),
      'source-no-css' => array(
        'files' => array(
          'js' => array(
            'js/jquery.themepunch.plugins.min.js',
            'js/jquery.themepunch.revolution.js',
          ),
        ),
        'variant arguments' => array(
          'variant' => 'source-no-css',
        ),
      ),
    ),
  );

  return $libraries;
}

/**
 * Implements hook_entity_info_alter().
 *
 * Add our revoltion slider bundle to fieldable panels pane.
 */
function slider_revolution_entity_info_alter(&$entity_info) {
  $entity_info['fieldable_panels_pane']['bundles']['slider_revolution'] = array(
    'label' => t('Slider revolution'),
    'pane category' => t('Slideshow'),
    'admin' => array(
      'path' => 'admin/structure/fieldable-panels-panes/manage/%fieldable_panels_panes_type',
      'bundle argument' => 4,
      'real path' => 'admin/structure/fieldable-panels-panes/manage/slider-revolution',
      //'access callback' => FALSE,
      //'access arguments' => array(NULL),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function slider_revolution_menu() {
  $items['admin/structure/fieldable-panels-panes/manage/%/import'] = array(
    'title' => 'Import slider from Wordpress',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('slider_revolution_slider_import'),
    'access callback' => 'slider_revolution_panes_access',
    'access arguments' => array('create', 4),
    'file' => 'slider_revolution.import.inc',
    'type' => MENU_LOCAL_ACTION,
  );
  return $items;
}

function slider_revolution_preprocess_fieldable_panels_pane(&$variables) {
  if ($variables['elements']['#bundle'] == 'slider_revolution') {
    $settings = field_get_items($variables['elements']['#entity_type'], $variables['elements']['#element'], 'slider_revolution_settings');
    $settings = $settings ? reset($settings): FALSE;

    if ($settings) {
      // Set up render variables.
      $variables['slider_id'] = drupal_html_id('rev-slider-' . $variables['elements']['#element']->fpid);
      $variables['settings'] = isset($settings['settings']['_internal']) ? $settings['settings']['_internal']: array();

      // Load the required libraries.
      $variant = !empty($variables['settings']['debug']) ? 'source-': '';
      $variant .= empty($variables['settings']['load_css']) ? 'no-css-': '';
      $variant = empty($variant) ? NULL: trim($variant, '-');
      libraries_load('slider_revolution', $variant);

      // Output our settings.
      $plugin_settings = $settings['settings'];
      unset($plugin_settings['_internal']);
      drupal_add_js('(function($){$(document).ready(function(){
        $("#' . $variables['slider_id'] . '").revolution(' . json_encode($plugin_settings) . ');
      })})(jQuery)', 'inline');

      // Add in our CSS.
      $css = "#{$variables['slider_id']} { width: {$settings['settings']['startwidth']}px; height: {$settings['settings']['startheight']}px; position: relative; overflow: hidden; }";
      if (isset($variables['settings']['custom_css'])) {
        $css .= ' ' . trim($variables['settings']['custom_css']);
      }
      drupal_add_css($css, array('type' => 'inline'));
    }
  }
}

function slider_revolution_theme($existing, $type, $path) {
  return array(
    'fieldable_panels_pane__slider_revolution' => array(
      'render element' => $existing['fieldable_panels_pane']['render element'],
      'preprocess functions' => $existing['fieldable_panels_pane']['preprocess functions'],
      'process functions' => $existing['fieldable_panels_pane']['process functions'],
      'base hook' => 'fieldable_panels_pane',
      'template' => 'fieldable-panels-pane--slider-revolution',
    ),
    'field__slider_revolution_slides' => array(
      'render element' => $existing['field']['render element'],
      'preprocess functions' => $existing['field']['preprocess functions'],
      'process functions' => $existing['field']['process functions'],
      'base hook' => 'field',
      'template' => 'field--slider-revolution-slides',
    ),
  );
}

/**
 * Implement hook_field_info().
 */
function slider_revolution_field_info() {
  return array(
    'slider_revolution_settings' => array(
      'label' => t('Slider revolution settings'),
      'description' => t('Contains the settings for the slider revolution.'),
      'settings' => array(),
      'instance_settings' => array(),
      'default_widget' => 'slider_revolution_settings_widget',
      'default_formatter' => 'slider_revolution_settings_formatter',
      'no_ui' => TRUE,
    ),
  );
}

/**
 * Implements hook_field_is_empty().
 */
function slider_revolution_field_is_empty($item, $field) {
  return empty($item['settings']);
}

/**
 * Implements hook_field_widget_info().
 */
function slider_revolution_field_widget_info() {
  return array(
    'slider_revolution_settings_widget' => array(
      'label' => t('Slider revolution settings'),
      'description' => t('Contains the settings for the slider revolution.'),
      'field types' => array('slider_revolution_settings'),
      'settings' => array(),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function slider_revolution_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $element['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('General Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => !empty($items[$delta]),
  );

  $element['settings']['delay'] = array(
    '#type' => 'textfield',
    '#title' => t('Delay'),
    '#description' => t('The time one slide stays on the screen.'),
    '#default_value' => isset($items[$delta]['settings']['delay']) ? $items[$delta]['settings']['delay']: 9000,
    '#field_suffix' => 'milliseconds',
    '#required' => TRUE,
    '#size' => 4,
  );

  $element['settings']['startwidth'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => isset($items[$delta]['settings']['startwidth']) ? $items[$delta]['settings']['startwidth']: 890,
    '#field_suffix' => 'px',
    '#required' => TRUE,
    '#size' => 4,
  );

  $element['settings']['startheight'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => isset($items[$delta]['settings']['startheight']) ? $items[$delta]['settings']['startheight']: 490,
    '#field_suffix' => 'px',
    '#required' => TRUE,
    '#size' => 4,
  );

  $element['settings']['navigationType'] = array(
    '#type' => 'radios',
    '#title' => t('Navigation Type'),
    '#default_value' => isset($items[$delta]['settings']['navigationType']) ? $items[$delta]['settings']['navigationType']: 'none',
    '#options' => array(
      'none' => t('No Navigation'),
      'bullet' => t('Bullets'),
      'thumb' => t('Thumbnails'),
      'both' => t('Both'),
    ),
    '#required' => TRUE,
  );

  $element['settings']['_internal']['bannertimer'] = array(
    '#type' => 'select',
    '#title' => t('Banner Timer Position'),
    '#default_value' => isset($items[$delta]['settings']['_internal']['bannertimer']) ? $items[$delta]['settings']['_internal']['bannertimer']: NULL,
    '#empty_option' => t('Hidden'),
    '#options' => array(
      'top' => t('Show at top'),
      'bottom' => t('Show at bottom'),
    ),
  );

  $element['settings']['_internal']['load_css'] = array(
    '#type' => 'radios',
    '#title' => t('Load standard slider revolution CSS'),
    '#default_value' => isset($items[$delta]['settings']['_internal']['load_css']) ? $items[$delta]['settings']['_internal']['load_css']: 1,
    '#options' => array(
      1 => t('Yes'),
      0 => t('No'),
    ),
  );
  $element['settings']['_internal']['custom_css'] = array(
    '#type' => 'textarea',
    '#title' => t('Custom CSS'),
    '#description' => t('Enter an custom CSS that will be loaded with this slider.'),
    '#default_value' => isset($items[$delta]['settings']['_internal']['custom_css']) ? $items[$delta]['settings']['_internal']['custom_css']: NULL,
  );
  if (!empty($form['#entity']->fpid)) {
    $element['settings']['_internal']['custom_css']['#description'] .= ' ' . t('It is recommended to prefix any selectors with %selector to prevent conflicts.', array(
      '%selector' => '#' . drupal_html_id('rev-slider-' . $form['#entity']->fpid),
    ));
  }

  if (isset($items[$delta]['settings'])) {
    $exposed_settings = array_keys($element['settings']);
    foreach ($items[$delta]['settings'] as $key => $value) {
      if (!in_array($key, $exposed_settings)) {
        $element['settings'][$key] = array(
          '#type' => 'value',
          '#value' => $value,
        );
      }
    }
  }

  return $element;
}

/**
 * Implements hook_field_storage_pre_insert().
 */
function slider_revolution_field_storage_pre_insert($entity_type, $entity) {
  _slider_revolution_serizalize_fields($entity_type, $entity);
}

/**
 * Implements hook_field_storage_pre_update().
 */
function slider_revolution_field_storage_pre_update($entity_type, $entity) {
  _slider_revolution_serizalize_fields($entity_type, $entity);
}

/**
 * Implements hook_field_attach_insert().
 */
function slider_revolution_field_attach_insert($entity_type, $entity) {
  _slider_revolution_serizalize_fields($entity_type, $entity, FALSE);
}

/**
 * Implements hook_field_attach_update().
 */
function slider_revolution_field_attach_update($entity_type, $entity) {
  _slider_revolution_serizalize_fields($entity_type, $entity, FALSE);
}

/**
 * Implements hook_field_load().
 */
function slider_revolution_field_load($entity_type, $entities, $field, $instances, $langcode, &$items, $age) {
  // Unserialize settings arrays.
  foreach ($entities as $id => $entity) {
    foreach ($items[$id] as $delta => $item) {
      // Unserialize the settings array if necessary.
      if (!empty($items[$id][$delta]['settings']) && !is_array($items[$id][$delta]['settings'])) {
        $items[$id][$delta]['settings'] = unserialize($items[$id][$delta]['settings']);
      }
    }
  }
}

/**
 * Serialize or unserialze field settings.
 *
 * @param $entity_type
 * @param $entity
 * @param $serialize
 *   Whether we want to serialize (TRUE) or unserialize (FALSE).
 */
function _slider_revolution_serizalize_fields($entity_type, $entity, $serialize = TRUE) {
  list(, , $bundle) = entity_extract_ids($entity_type, $entity);
  $instances = field_info_instances($entity_type, $bundle);
  $fields = field_info_fields();

  // Iterate through finding any slider_revolution_settings fields.
  foreach ($instances as $instance) {
    $field_name = $instance['field_name'];

    if ($fields[$field_name]['type'] == 'slider_revolution_settings' && isset($entity->{$field_name})) {
      foreach ($entity->{$field_name} as $langcode => &$items) {
        foreach ($items as $delta => &$item) {
          if (isset($item['settings'])) {
            if ($serialize && is_array($item['settings'])) {
              $item['settings'] = serialize($item['settings']);
            }
            elseif (!$serialize && is_string($item['settings'])) {
              $item['settings'] = unserialize($item['settings']);
            }
          }
        }
      }
    }
  }
}

/**
 * Perform access checks only allowing the slider_revolution bundle.
 *
 * @see fieldable_panels_panes_access()
 */
function slider_revolution_panes_access($op, $entity = NULL, $account = NULL) {
  // Sniff out our bundle.
  $bundle = NULL;
  if (is_object($entity)) {
    list(, , $bundle) = entity_extract_ids('fieldable_panels_pane', $entity);
  }
  elseif (is_string($entity)) {
    $bundle = strtr($entity, array('-' => '_'));
  }

  return $bundle == 'slider_revolution' ? fieldable_panels_panes_access($op, $entity, $account): FALSE;
}
