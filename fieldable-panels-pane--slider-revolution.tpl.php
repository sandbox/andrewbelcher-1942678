<div class="banner-container">
  <div class="banner" id="<?php print $slider_id; ?>">
    <?php print render($content); ?>
    <?php if (!empty($settings['bannertimer'])) : ?>
    <div class="tp-bannertimer<?php if ($settings['bannertimer'] == 'bottom') print ' tp-bottom'; ?>"></div>
    <?php endif; ?>
  </div>
</div>
